<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/indexStyle.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="./index.css">
    <link rel="stylesheet" href="./footer.css">
    <!--<link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">-->
</head>

<body>
    <div class="container" id="c">
        <div class="aling-items-start menu #content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <img src="./resources/img-los.png" alt="" width="120" height="50">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                            </li>
                        </ul>
                        <form class="d-flex">
                            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </nav>
        </div>


        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="./resources/slider.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>First slide label</h5>
                        <p>Some representative placeholder content for the first slide.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./resources/off.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Second slide label</h5>
                        <p>Some representative placeholder content for the second slide.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./resources/linux.-slider.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Third slide label</h5>
                        <p>Some representative placeholder content for the third slide.</p>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>


        <div class="row mt-2">
            <section class="col-md-9 ">
                <h1 class="col-md-12 text-center h1title">
                    Aprende Docker
                </h1>
                <div class="row">
                    <div class="col mb-6">
                        <div class="card border-dark mb-3" style="max-width: 25rem;">
                            <div class="card-header">Introduccion a Docker</div>
                            <div class="card-body text-dark">
                                <h5 class="card-title">Que es Docker</h5>
                                <p class="card-text">Docker es una plataforma abierta para desarrollar, enviar y ejecutar aplicaciones., que nos permite separar las aplicaciones de su infraestructura para que se pueda enviar software rápidamente.</p>
                            </div>
                        </div>
                        <div class="card border-dark mb-3" style="max-width: 25rem;">
                            <div class="card-header">Introduccion a Docker</div>
                            <div class="card-body text-dark">
                                <h5 class="card-title">La plataforma Docker</h5>
                                <p class="card-text">Docker proporciona la capacidad de empaquetar y ejecutar una aplicación en un entorno poco aislado llamado contenedor. El aislamiento y la seguridad permiten ejecutar muchos contenedores simultáneamente en un host determinado.</p>
                            </div>
                        </div>
                        <div class="card border-dark mb-3" style="max-width: 25rem;">
                            <div class="card-header">Introduccion a Docker</div>
                            <div class="card-body text-dark">
                                <h5 class="card-title">Arquitectura de Docker</h5>
                                <p class="card-text">¿Docker usa una arquitectura cliente-servidor. El cliente Docker habla con el daemon Docker, que hace el trabajo pesado de construir, ejecutar y distribuir sus contenedores Docker. El cliente Docker y el daemon pueden ejecutarse en el mismo sistema, o se puede conectar un cliente Docker a un daemon Docker remoto.</p>
                            </div>
                        </div>
                       
                    </div>

                    <div class="col mb-6">

                    <div class="card border-dark mb-3" style="max-width: 25rem;">
                            <div class="card-header">Introduccion a Docker</div>
                            <div class="card-body text-dark">
                                <h5 class="card-title">Registros de Docker</h5>
                                <p class="card-text">Un registro de Docker almacena imágenes de Docker. Docker Hub y Docker Cloud son registros públicos que cualquiera puede usar, configurado ais para buscar imagenes. Compilado imagenes como ubunto</p>
                            </div>
                        </div>

                        <div class="card border-dark mb-3" style="max-width: 25rem;">
                            <div class="card-header">Introduccion a Docker</div>
                            <div class="card-body text-dark">
                                <h5 class="card-title">Que es Docker</h5>
                                <p class="card-text">Una imagen es una plantilla de 'solo lectura' con instrucciones para crear un contenedor Docker. A menudo, una imagen se basa en otra imagen, con alguna personalización adicional. </p>
                            </div>
                        </div>
                        <div class="card border-dark mb-3" style="max-width: 25rem;">
                            <div class="card-header">Introduccion a Docker</div>
                            <div class="card-body text-dark">
                                <h5 class="card-title">Contenedores</h5>
                                <p class="card-text">Un contenedor es una instancia ejecutable de una imagen. Se puede crear, iniciar, detener, mover o eliminar un contenedor utilizando la API Docker o el CLI. Se puede conectar un contenedor a una o más redes, adjuntarle almacenamiento o incluso crear una nueva imagen en función de su estado actual.</p>
                            </div>
                        </div>
                    </div>


                </div>

            </section>


            <aside class="col-md-3 d-none d-md-block mt-5">
                <div class="row">
                    <img src="./resources/publicidad-1.PNG" alt="" width="80" height="400">
                    <img src="./resources/publicidad-2.PNG" alt="" width="80" height="400">

                </div>

            </aside>
        </div>
    </div>
    <!--Pie de Pagina-->
    <!-- Site footer -->
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <h6>About</h6>
                    <p class="text-justify">Docker <i>Implementamos php como </i> una imagen basada en la imagen ubuntu, pero que instala el servidor web Apache y nuestra aplicación, así como los detalles de configuración necesarios para ejecutar nuestra aplicación.</p>
                </div>

                <div class="col-xs-6 col-md-3">
                    <h6>Categories</h6>
                    <ul class="footer-links">
                        <li><a href="http://scanfcode.com/category/c-language/">C</a></li>
                        <li><a href="http://scanfcode.com/category/front-end-development/">UI Design</a></li>
                        <li><a href="http://scanfcode.com/category/back-end-development/">PHP</a></li>
                        <li><a href="http://scanfcode.com/category/java-programming-language/">Java</a></li>
                        <li><a href="http://scanfcode.com/category/android/">Android</a></li>
                        <li><a href="http://scanfcode.com/category/templates/">Templates</a></li>
                    </ul>
                </div>

                <div class="col-xs-6 col-md-3">
                    <h6>Quick Links</h6>
                    <ul class="footer-links">
                        <li><a href="http://scanfcode.com/about/">About Us</a></li>
                        <li><a href="http://scanfcode.com/contact/">Contact Us</a></li>
                        <li><a href="http://scanfcode.com/contribute-at-scanfcode/">Contribute</a></li>
                        <li><a href="http://scanfcode.com/privacy-policy/">Privacy Policy</a></li>
                        <li><a href="http://scanfcode.com/sitemap/">Sitemap</a></li>
                    </ul>
                </div>
            </div>
            <hr>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-6 col-xs-12">
                    <p class="copyright-text">Copyright &copy; 2017 All Rights Reserved by
                        <a href="#">Grupo-4 LSO2</a>.
                    </p>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <ul class="social-icons">
                        <li><a class="facebook" href="#"><i class="fab fa-facebook"></i></a></li>
                        <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></i></a></li>
                        <li><a class="dribbble" href="#"><i class="fab fa-dribbble-square"></i></a></li>
                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    </div>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"> </script>
</body>

</html>